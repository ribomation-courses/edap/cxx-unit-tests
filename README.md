C++ Unit Tests, 1 day
====

Welcome to this course.
Here you will find
* Installation instructions
* Support files
* Demo programs
* Solutions to the programming exercises

Usage
====

You need to have a GIT client installed to clone this repo. Otherwise, you can just click on the download button and grab it all as a ZIP or TAR bundle.

* [GIT Client Download](https://git-scm.com/downloads)

Get the sources initially by a GIT clone operation

    git clone https://gitlab.com/ribomation-courses/edap/cxx-unit-tests.git
    cd cxx-unit-tests

Get the latest updates by a GIT pull operation

    git pull

Installation Instructions
====

It's the same installation instructions as for the
[C++ Basics](https://gitlab.com/ribomation-courses/edap/cxx-basics-5days) course.

In addition, you will during the beginning of the course install Google Test.

Ensure you have the following tools installed:
* GCC/G++ version 7 or later
* make
* cmake
* Jetbrains CLion


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
