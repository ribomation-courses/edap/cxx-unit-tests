#pragma  once

#include <iostream>
#include <string>
#include <regex>

using namespace std;
using namespace std::string_literals;


inline bool
checkEmail(const string& email) {
    const auto atPos = email.find("@"s);
    if (atPos == string::npos) return false;

    auto idx = email.find("@"s, atPos + 1);
    if (idx != string::npos) return false;

    const auto dotPos = email.rfind("."s);
    if (dotPos == string::npos) return false;

    string tld    = email.substr(dotPos + 1);
    string domain = email.substr(atPos + 1, dotPos - atPos - 1);
    string name   = email.substr(0, atPos);
    cout << "\ntld=" << tld << ", domain=" << domain << ", name=" << name << endl;

    if (tld.size() < 2) return false;

    regex re{"[a-z][a-z0-9-]*"};
    if (!regex_match(tld, re)) return false;
    if (!regex_match(domain, re)) return false;

    regex re2{"[a-z][a-z0-9.-]*"};
    if (!regex_match(name, re2)) return false;

    return true;
}

