#include "gtest/gtest.h"
#include "check-email.hxx"
#include <string>

using namespace testing;
using namespace std;
using namespace std::string_literals;


struct EmailValues : TestWithParam<string> {};
INSTANTIATE_TEST_CASE_P(email, EmailValues, Values(
        "jens.riboe@ribomation.se"s, "nisse@foobar.com"s
));
TEST_P(EmailValues, validEmails) {
    auto email = GetParam();
    EXPECT_TRUE(checkEmail(email));
}



struct EmailValues2 : TestWithParam<string> {};
INSTANTIATE_TEST_CASE_P(email2, EmailValues2, Values(
        "jens.riboe@@ribomation.se"s, "@foobar.com"s
));
TEST_P(EmailValues2, validEmails2) {
    auto email = GetParam();
    EXPECT_FALSE(checkEmail(email));
}


