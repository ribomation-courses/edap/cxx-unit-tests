#pragma  once


template<typename ElemType>
class Queue {
    const unsigned capacity;
    ElemType* q;
    int putIdx = 0, getIdx = 0, numElems = 0;

public:
    Queue(unsigned max = 10)
            : capacity{max}, q{new ElemType[capacity]} {
    }

    ~Queue() {
        delete[] q;
    }

    unsigned size() const { return static_cast<unsigned >(numElems); }
    bool empty() const { return size() == 0; }
    bool full() const { return size() == capacity; }

    void put(ElemType x) {
        q[putIdx++] = x;
        putIdx %= capacity;
        ++numElems;
    }

    ElemType get() {
        ElemType x = q[getIdx++];
        getIdx %= capacity;
        --numElems;
        return x;
    }
};

