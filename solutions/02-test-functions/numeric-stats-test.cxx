#include "gtest/gtest.h"

#include <tuple>
#include <vector>
#include <numeric>
#include <algorithm>
#include <stdexcept>

using namespace std;
using namespace std::string_literals;
using namespace testing;

// --- target ---
template<typename T>
tuple<double, T, T>
numeric_stats(const vector<T>& data) {
    if (data.empty()) {
        throw invalid_argument("empty data vector<T>");
    }

    double sum = accumulate(data.begin(), data.end(), 0);
    T      min = *min_element(data.begin(), data.end());
    T      max = *max_element(data.begin(), data.end());

    return make_tuple(sum / data.size(), min, max);
}
// --- END ---


TEST(integral, allTen) {
    vector<int> data{10, 10, 10, 10, 10};

    auto[avg, min, max] = numeric_stats(data);

    EXPECT_EQ(avg, 10);
    EXPECT_EQ(min, 10);
    EXPECT_EQ(max, 10);
}

TEST(integral, oneToFive) {
    vector<int> data{1, 2, 3, 4, 5};

    auto[avg, min, max] = numeric_stats(data);

    EXPECT_DOUBLE_EQ(avg, 3);
    EXPECT_EQ(min, 1);
    EXPECT_EQ(max, 5);
}

TEST(integral, empty) {
    vector<int> data;
    EXPECT_THROW(numeric_stats(data), invalid_argument);
}


TEST(floatingPoint, oneToFive) {
    vector<float> data{1.F, 2.F, 3.F, 4.F, 5.F};

    auto[avg, min, max] = numeric_stats(data);

    EXPECT_DOUBLE_EQ(avg, 3);
    EXPECT_EQ(min, 1.F);
    EXPECT_EQ(max, 5.F);
}

