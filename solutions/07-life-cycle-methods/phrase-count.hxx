#pragma once

#include <iostream>
#include <string>

using namespace std;

inline unsigned
count(const string& phrase, const string& payload) {
    unsigned  cnt = 0;
    for (auto pos = payload.find(phrase); (pos = payload.find(phrase, pos + phrase.size())) != string::npos; ++cnt) {
    }
    return cnt == 0 ? 0 : cnt + 1;
}


