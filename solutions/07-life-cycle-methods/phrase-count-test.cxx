#include "gtest/gtest.h"
#include "phrase-count.hxx"
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <chrono>

using namespace std;
using namespace std::string_literals;
using namespace std::chrono;
using namespace testing;

struct SuiteFixture : Environment {
    static string                  payload;
    const system_clock::time_point startTime = system_clock::now();

    void SetUp() override {
        const auto filename = "../Shakespeare.txt"s;
        fstream    f{filename};
        if (!f) throw invalid_argument("cannot open " + filename);

        f.seekg(0, ios_base::end);
        unsigned long size = static_cast<unsigned long>(f.tellg());

        f.seekg(0, ios_base::beg);
        char buf[size];
        f.read(buf, size);

        string s{buf, size};
        payload = s;
        ASSERT_FALSE(payload.empty());
    }

    void TearDown() override {
        auto endTime     = high_resolution_clock::now();
        auto elapsedTime = duration_cast<microseconds>(endTime - startTime).count();
        cout << "*** Total elapsed time: " << elapsedTime << " us" << endl;
    }
};

string SuiteFixture::payload;

TEST(count, hamlet) {
    EXPECT_EQ(count("Hamlet"s, SuiteFixture::payload), 113U);
}

TEST(count, richard) {
    EXPECT_EQ(count("Richard"s, SuiteFixture::payload), 182U);
}

TEST(count, juliet) {
    EXPECT_EQ(count("Juliet"s, SuiteFixture::payload), 72U);
}


int main(int argc, char** argv) {
    InitGoogleTest(&argc, argv);
    AddGlobalTestEnvironment(new SuiteFixture{});
    return RUN_ALL_TESTS();
}


