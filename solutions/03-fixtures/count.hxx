#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <tuple>
#include <stdexcept>

using namespace std;
using namespace std::string_literals;

inline
tuple<unsigned, unsigned, unsigned>
count(const string& filename) {
    ifstream f{filename};
    if (!f) throw invalid_argument("cannot open "s + filename);

    unsigned lines = 0;
    unsigned words = 0;
    unsigned chars = 0;
    for (string line; getline(f, line); ++lines) {
        chars += line.size();
        istringstream buf{line};
        for (string w; buf >> w; ++words) ;
    }

    return make_tuple(lines, words, chars);
}
