#include <string>
#include <fstream>
#include <unistd.h>
#include "gtest/gtest.h"
#include "count.hxx"

using namespace std;
using namespace std::string_literals;
using namespace testing;


struct ExternalFile : Test {
    const string filename = "../count.hxx";
    ExternalFile() {
        ifstream f{filename};
        if (!f) throw invalid_argument("cannot find test file "s + filename);
    }
};

TEST_F(ExternalFile, simpleCount) {
    auto[lines, words, chars] = count(filename);
    EXPECT_EQ(lines, 27U);
    EXPECT_EQ(words, 74U);
    EXPECT_EQ(chars, 597U);
}


struct GeneratedFile : Test {
    const string filename = "/tmp/test-data.txt";
protected:
    void SetUp() override {
        ofstream f{filename};
        f << "this is the 1st line" << endl;
        f << "here comes the 2nd line" << endl;
        f << "let's throw in a 3rd line for good measures" << endl;
    }

    void TearDown() override {
        ASSERT_FALSE(unlink(filename.c_str()));
    }
};

TEST_F(GeneratedFile, simpleCount) {
    auto[lines, words, chars] = count(filename);

    EXPECT_EQ(lines, 3U);
    EXPECT_EQ(words, 19U);
    EXPECT_EQ(chars, 86U);
}

